import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LyricsComponent } from './components/lyrics/lyrics.component';
import { MainComponent } from './components/main/main.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PlayerComponent } from './components/player/player.component';
import { AboutComponent } from './components/about/about.component';

// определение дочерних маршрутов
const itemRoutes: Routes = [
  { path: 'about', component: AboutComponent},
  { path: 'lyrics/:track', component: LyricsComponent},
];

const routes: Routes = [
  { path: '', component: MainComponent, children: itemRoutes},
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
