import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Track, AudioPlayerComponent} from 'ngx-audio-player';
import { MyTrackModule } from 'src/app/modules/mytrack.module';


@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  showSongText: boolean = true;

  myTrackModule: MyTrackModule = new MyTrackModule();

  @ViewChild("audioPlayer") pleer: AudioPlayerComponent | undefined;

  currentIndex: number = -1;

  autoPlay = false;
  msaapDisplayTitle = true;
  msaapDisplayPlayList = true;
  msaapPageSizeOptions = [1,2,4,6];
  msaapDisplayVolumeControls = true;
  msaapDisplayRepeatControls = true;
  msaapDisplayArtist = false;
  msaapDisplayDuration = false;
  msaapDisablePositionSlider = false;

  // Material Style Advance Audio Player Playlist
  msaapPlaylist: Track[] = this.myTrackModule.msaapPlaylist;

  constructor(private router: Router) {
  }


  @HostListener("change") myChange() {
   console.log('change');

  }
  @HostListener("click") myClick() {
    console.log(this.pleer);


    console.log(this.pleer?.currentIndex);
    if (this.pleer != undefined){
      console.log(this.msaapPlaylist[this.pleer.currentIndex].title);
      console.log(this.pleer.isPlaying);


      if (this.currentIndex != this.pleer.currentIndex){
        this.currentIndex = this.pleer.currentIndex;
        let currentLabel = this.myTrackModule.msaapPlaylist[this.currentIndex].label;

        console.log('Смена трека');

        if (this.showSongText){
          this.router.navigate([`/lyrics/${currentLabel}`])
        }
      }
    }

}

  ngOnInit( ): void {
    setTimeout(() => {
      this.audioPlayerConfiguration(this.pleer);
    }, 100);
  }

  audioPlayerConfiguration(pleer: AudioPlayerComponent | undefined){
    if (pleer != undefined){
      pleer.titleHeader = 'Название';
      pleer.durationHeader = 'Дительность';
      pleer.tableHeader = 'Плейлист';
      pleer.paginator._intl.itemsPerPageLabel = "Количество песен на странице";

      var max = this.msaapPageSizeOptions.reduce((prev, cur) => {
        if (prev > cur) {
          return prev
        }
        return cur
      })
      pleer.dataSource.paginator?._changePageSize(max);
      console.log(pleer);
    }
  }


  onChanged(increased:any){
    console.log(increased);
  }


}
