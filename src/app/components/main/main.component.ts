import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { setTokenSourceMapRange } from 'typescript';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.showPlayer = true;
    }, 1000);
  }

  title = 'DREY-PLAY';
  showPlayer = false;

  goToStartPage(){
     this.router.navigate(['/about']);
  }

}
