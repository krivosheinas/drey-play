import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router : Router)
  {
    console.log(this.route);
    console.log(this.router)
  }

  ngOnInit(): void {
    if (this.router.url == "/"){
        this.router.navigate(['/about']);
    }
  }




}
