import { Component, OnInit, SimpleChanges } from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MyTrackModule } from 'src/app/modules/mytrack.module';

@Component({
  selector: 'app-lyrics',
  templateUrl: './lyrics.component.html',
  styleUrls: ['./lyrics.component.scss']
})
export class LyricsComponent implements OnInit {

  myTrackModule: MyTrackModule = new MyTrackModule();

  track : string | null = null;

  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute){
    this.routeSubscription = route.params.subscribe(params=>{
      this.track = params['track'];
      console.log('subscribe');
    });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

  }





}
