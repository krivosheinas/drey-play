import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyTrack } from './mytrack.model';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MyTrackModule {

  msaapPlaylist: MyTrack[] = [
    {
      title: 'Emotion',
      label: 'emotion',
      link: 'assets/tracks/Emotion.mp3',
      artist: 'Audio One Artist',
      duration: 163
    },
    {
      title: 'Emotion Mix',
      label: 'emotion_mix',
      link: 'assets/tracks/Emotion (mix).mp3',
      artist: 'Audio One Artist',
      duration: 165
    },
    {
      title: 'Банки',
      label: 'banki',
      link: 'assets/tracks/Банки.mp3',
      artist: 'Audio One Artist',
      duration: 165
    }
  ];

 }
